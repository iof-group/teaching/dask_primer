## Dask primer for larger-than-memory image analysis
---

AIAI meeting 10/06/2024

Christoph Sommer (ISTA)

## Requirements

#### pip dependencies

* numpy
* matplotlib
* scipy
* stackview
* dask
* dask-image
* scikit-image
